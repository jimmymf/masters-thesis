# Items of interest
items_of_interest <- function(active_user_id, user_interactions_data, properties_df){
  #' Items of interest to the active-user
  #' 
  #' Return items that could be interesting for the active user, based on items that have interest in the same items as the active user and on items that are in the district. 
  #' 
  #' @param active_user_id str. String of the active user. 
  #' @param user_interactions_data dataframe. Dataframe which expresses users interactions with items.
  #' @param properties_df dataframe. Dataframe which contains all the properties and their attributes. 
  #'  
  #'  @returns list with 3 items: user_items = items that are interesting for the active user (training), collaborative_items = items that are interesting for **similar** users and items_in_city = items that are in the same district(s) of the previous items. 
  #'  
  
  # Items liked by users
  likes_df <- user_interactions_data %>% filter(seen > 0 | suggest_similar > 0) %>% select(c(user_id, item_id, seen, suggest_similar))
  
  superlikes_df <- user_interactions_data %>% filter(((seen>0) + (suggest_similar>0)) == 2) %>% select(c(user_id, item_id, seen))
  
  training_items <- likes_df %>% filter(user_id == active_user_id) %>% select(item_id) %>% deframe() %>% as.character()
  
  collaborative_users <- likes_df %>% filter(item_id %in% training_items) %>% select(user_id) %>% distinct() %>% deframe() 
  
  collaborative_items <- superlikes_df %>% filter(user_id %in% collaborative_users) %>% filter(!(item_id %in% training_items)) %>% select(item_id) %>% distinct() %>% deframe() %>% as.character()
  
  ## ALTERNATIVE
  # collaborative_items <- likes_df %>% filter(user_id %in% collaborative_users) %>% filter(!(item_id %in% training_items)) %>% select(item_id) %>% distinct() %>% deframe() %>% as.character()
  
  collaborative_items_districts <- properties_df %>% filter(item_id %in% collaborative_items) %>% select(district_uuid) %>% distinct() %>% deframe()
  
  district_items <- properties_df %>% filter(district_uuid %in% collaborative_items_districts) %>% filter(!(item_id %in% collaborative_items)) %>% filter(!(item_id %in% training_items)) %>% select(item_id)
  
  if(nrow(district_items) > 300){
    district_items <- district_items %>% sample_n(300)
  }
  
  district_items <- district_items %>% deframe() %>% as.character()
  
  return(list(user_items = training_items, collaborative_items = collaborative_items, items_in_city = district_items))
}
