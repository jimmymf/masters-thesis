test_ncssvm <- function(active_user, user_interactions_ds, properties_df, n_gammas = 10, 
                        indep_variables = c("monthly_rent", "unit_area", "room_qty"), 
                        hkernelfunc = kernlab::rbfdot, kernelparam = list(sigma = 0.5)){
  
  # Get items of interest
  ioi <- items_of_interest(active_user_id = active_user, user_interactions_data = user_interactions_ds, properties_df  = properties_df)
  
  # Get data matrix
  datamatrix <- get_data_matrix(items_interest_list = ioi, properties_data = properties_data_trim, indep_variables = indep_variables)
  
  #test$x[,c(1:2,5)] <- scale(test$x[,c(1:2,5)])
  datamatrix$x <- scale(datamatrix$x) # Check when to scale data and which variables to scale
  
  lambda_test <- cssvm_lambdamax(x = datamatrix$x, y = datamatrix$y, kernelfunc = hkernelfunc, kernelparam = unlist(kernelparam)) * 0.5 # Which value to use here? 
  
  gamma_test <- append(seq(from=0.5, to=0.8, by=0.1), pracma::linspace(0.81, 1, n_gammas))
  
  result_test <- ncssvm_wb(x = datamatrix$x, y = datamatrix$y, 
                           lambda = lambda_test, gamma =gamma_test, 
                           hkernelfunc = hkernelfunc, kernelparam = kernelparam)
  
  return(list(nvssvm_results = result_test, user_items = datamatrix$items_interest, gammas = gamma_test))
}

