## Algorithm Low density boundary selection

lowDensityAlg <- function(list_tests){
  
  n_r <- sum(list_tests[[1]]$user_items$set!="Training") # Number of property proposals
  k_vector <- c(ceiling(n_r*0.08), ceiling(n_r*0.1), ceiling(n_r*0.12))
  
  #Create empty list
  DC_df <- list()
  for (test_idx in seq_along(list_tests)){
    DC_df_test <- list()
    if(purrr::is_empty(k_vector)) break
    for (k in seq_along(k_vector)){
      DC_df_k <- list()
      f_values <- list_tests[[test_idx]]$nvssvm_results$fk
      # Search which gamma have enough labels +1 and -1  
      over_0 <- apply(f_values, 2, function(x) sum(x>0))
      below_0 <- apply(f_values, 2, function(x) sum(x<0))
      col_indices <- which((over_0 > k_vector[k]) & (below_0 > k_vector[k]))
      if (purrr::is_empty(col_indices)) break
      # Go through each of the valid gammas
      for(gamma_idx in col_indices){
        Hplus_minus <- f_values[, gamma_idx]
        plus_indices <- which(Hplus_minus > 0)
        minus_indices <- which(Hplus_minus < 0)
        # Sort from closest to furthest
        Hplus <- sort(Hplus_minus[plus_indices], decreasing = F)[1:k_vector[k]]
        Hminus <- sort(Hplus_minus[minus_indices], decreasing = T)[1:k_vector[k]]
        # Calculate euclidean distance
        eucl_distance <- sqrt((Hplus - Hminus)**2)
        # Add median to df
        DC_df_k <- dplyr::bind_rows(DC_df_k,
                                  c(test_n = test_idx, 
                                    k = k_vector[k], 
                                    DC = median(eucl_distance),
                                    gamma = list_tests[[test_idx]]$gammas[gamma_idx])
                                  ) 
      }
      DC_df_test <- dplyr::bind_rows(DC_df_test, DC_df_k[which.max(DC_df_k$DC), ])
    }
    DC_df <- dplyr::bind_rows(DC_df, DC_df_test[which.min(DC_df_test$DC), ])
  }
  return(DC_df %>% dplyr::arrange(test_n, desc(DC), k))
}

