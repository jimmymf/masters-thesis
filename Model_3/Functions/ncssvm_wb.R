ncssvm_wb <- function(x, y, hkernelfunc = kernlab::rbfdot, kernelparam = list(sigma = 0.5), lambda, gamma, alpha = NULL){
  #' From the paper "Nested Support Vector Machines" by Gyemin Lee and Clayton Scott.
  #' 
  #' "Translated" from their original matlab code in https://web.eecs.umich.edu/~cscott/code.html#nestedsvm to R. 
  #' 
  #' R code to generate cost-sensitive SVM that are properly nested (unlike standard SVMS) as the cost-asymmetry or density level parameter is varied. The solution paths are piecewise linear with a user-selected number of breakpoints. 
  #' 
  #' Find nested decision rules at multiple cost asymmetry at the same time
  #' 
  #' @param x N x p data
  #' @param y N x 1 label
  #' @param hkernelfunc Kernel function, see kernlab::dots.
  #' @param kernelparam list of Kernel parameters, see kernlab::dots
  #' @param lambda regularization parameter
  #' @param gamma M x 1 cost asymmetries of interest
  #' @param alpha N x M initial alphas at the cost asymmetry gamma
  #'  
  #'  @returns list with 3 items: 
  #'  @return alpha N x M parameters
  #'  @return fk decision function value; > 0 positive; < 0 negative
  #'  @return dual objective function value. 
  #'  
  
  
  y <- matrix(y, ncol = 1)
  gamma <- matrix(gamma, ncol = 1)
  
  tol1 <- 1e-9
  tol2 <- 1e-6
  tol <- tol1
  EPS <- 1e-10
  
  N <- dim(x)[1]
  M <- length(gamma)
  if (!is.null(unlist(kernelparam))) hkernel <- hkernelfunc(unlist(kernelparam))
  else hkernel <- hkernelfunc()
  K <- kernelMatrix(kernel = hkernel, x = x) ## Check kernel functions in packages
  K <- (K + t(K)) / 2
  Kt <- diag(c(y)) %*% K %*% diag(c(y))

  loopnum <- 0
  tolloopnum1 <- 5000
  maxloopnum <- 10000
  bStop <- FALSE
  prevD <- Inf
  # DD = pracma::zeros(maxloopnum,1);
  # pplst <- pracma::zeros(maxloopnum,1);
  # errlst = pracma::zeros(maxloopnum,1);
  
  
  # initialization
  
  if (is.null(alpha)) alpha <- pracma::repmat(as.matrix(as.numeric(y == -1), ncol = 1), m = M, n = 1) + (y %*% t(gamma))

  prevAlpha <- alpha
  preverr <- Inf
  prevpp <- 0
  bpp <- 0

  while(isFALSE(bStop)){
    loopnum <- loopnum +1
    
    # Print loop num
    if (loopnum %% 500 == 0){
      print(paste("Loop:",loopnum))
    }
    
    # Select a datapoint that violates the KKT condition most
    gim <- (Kt %*% alpha / lambda) -1
    pp_err <- selection(y, gamma, alpha, gim, bpp)
    pp <- pp_err[[1]]
    err <- pp_err[[2]]
    
    # Solve the subproblem
    anew <- t(alpha[pp,]) - lambda * t(gim[pp,]) /K[pp,pp]
    alpha[pp,] <- t(subproblem(y[pp], list(N,M), gamma, anew, t(alpha[pp,])))
    
    # Dual value
    D <- sum(diag(t(alpha) %*% Kt %*% alpha)) / (2*lambda) - sum(alpha)
    
    if(D > (prevD + tol)){
      print("WARNING: dual value increased")
      alpha <- prevAlpha
      bStop <- TRUE
    }else if (D <= (prevD + tol)){
      if ((err < tol) | (loopnum == maxloopnum)) bStop <- TRUE
      if (loopnum == tolloopnum1) tol <- tol2
      if(((abs(preverr-err)) < EPS) & ((max(abs(prevAlpha - alpha))) < EPS)){
        if(prevpp == pp) bpp <- 1
        else {
          print("WARNING: ncssvm_wb: sturated")
          bpp = bpp+1
          if (bpp >=N) bStop <- TRUE
        }
      }
      else bpp <- 0
      prevD <- D
      prevAlpha <- alpha
      prevpp <- pp
      preverr <- err
    }
    
  }
  print(paste('ncs_svm: loop:', loopnum, 'D:', D, 'err:', err))
  if(err > tol) print("<-----")
  
  fk <- K %*% (alpha * pracma::repmat(y, m = M, n = 1))
  dual <- sum(diag(t(alpha) %*% Kt %*% alpha)) / (2 * lambda) - sum(alpha)
  return(list(alpha = alpha, fk = fk, dual = dual))
}

selection <- function(y, gamma, alpha, gim, bpp){
  tol <- 1e-12
  N <- dim(alpha)[[1]]
  M <- dim(alpha)[[2]]
  Ip <- y == 1
  Im <- y == -1
  
  vim <- matrix(0,nrow = N, ncol = M)
  ferr <- matrix(0,nrow = N, ncol = M)

  # I+
  mm <- M
  idx1 <- alpha[,mm] < gamma[mm] - tol
  idx3 <- alpha[,mm-1] < alpha[,mm] - tol
  
  ferr[Ip & !idx1, mm] <- gim[Ip & !idx1, mm] * as.numeric(gim[Ip & !idx1,mm] > 0)
  vim[Ip & idx1 & !idx3, mm] <- gim[Ip & idx1 & !idx3, mm]
  ferr[Ip & idx1 & idx3, mm] <- abs(gim[Ip & idx1 & idx3, mm])
  
  for (mm in seq(from = M-1, to = 1, by = -1)){
    idx1 <- alpha[,mm] < (gamma[mm] - tol)
    idx2 <- alpha[,mm] < (alpha[,mm+1] - tol)
    if(mm>1) idx3 <- alpha[, mm-1] < (alpha[,mm] - tol)
    else idx3 <- tol < alpha[,mm]
    if (gamma[mm]==0) break
    
    ferr[Ip & !idx1 &  idx2,mm] <- gim[Ip & !idx1 &  idx2,mm]
    ferr[Ip & !idx1 & !idx2,mm] <- gim[Ip & !idx1 & !idx2,mm] + vim[Ip & !idx1 & !idx2,mm+1]
    ferr[Ip & !idx1,mm] <- ferr[Ip & !idx1,mm]*as.numeric(ferr[Ip & !idx1,mm]>0)
    
    vim[Ip &  idx1 &  idx2 & !idx3,mm] <- gim[Ip &  idx1 &  idx2 & !idx3,mm]
    vim[ Ip &  idx1 & !idx2 & !idx3,mm] <- gim[Ip &  idx1 & !idx2 & !idx3,mm] + vim[Ip &  idx1 & !idx2 & !idx3,mm+1]
    
    ferr[Ip &  idx1 &  idx2 &  idx3,mm] <- abs(gim[Ip &  idx1 &  idx2 &  idx3,mm])
    ferr[Ip &  idx1 & !idx2 &  idx3,mm] <- abs(gim[Ip &  idx1 & !idx2 &  idx3,mm] + vim[Ip &  idx1 & !idx2 &  idx3,mm+1])
  }
  
  #I-
  mm <- 1;
  idx1 <- alpha[,mm] < (1-gamma[mm] - tol)
  idx3 <- alpha[,mm+1] < (alpha[,mm] - tol)
  
  ferr[Im & !idx1,mm] <- gim[Im & !idx1,mm]* as.numeric(gim[Im & !idx1,mm]>0)
  vim[ Im &  idx1 & !idx3,mm] <- gim[Im &  idx1 & !idx3,mm]
  ferr[Im &  idx1 &  idx3,mm] <- abs(gim[Im &  idx1 &  idx3,mm])
  
  for (mm in 2:M){
    idx1 <- alpha[,mm] < (1-gamma[mm] - tol)
    idx2 <- alpha[,mm] < (alpha[ ,mm-1] - tol)
    if (mm<M) idx3 <- alpha[,mm+1] < (alpha[ ,mm]  - tol)
    else idx3 = tol < alpha[,mm]
    if (gamma[mm]==1) break
    
    ferr[Im & !idx1 &  idx2,mm] <- gim[Im & !idx1 &  idx2,mm]
    ferr[Im & !idx1 & !idx2,mm] <- gim[Im & !idx1 & !idx2,mm] + vim[Im & !idx1 & !idx2,mm-1]
    ferr[Im & !idx1,mm] <- ferr[Im & !idx1,mm]*(ferr[Im & !idx1,mm]>0)
    
    vim[ Im &  idx1 &  idx2 & !idx3,mm] <- gim[Im &  idx1 &  idx2 & !idx3,mm]
    vim[ Im &  idx1 & !idx2 & !idx3,mm] <- gim[Im &  idx1 & !idx2 & !idx3,mm] + vim[Im &  idx1 & !idx2 & !idx3,mm-1]
    
    ferr[Im &  idx1 &  idx2 &  idx3,mm] <- abs(gim[Im &  idx1 &  idx2 &  idx3,mm])
    ferr[Im &  idx1 & !idx2 &  idx3,mm] <- abs(gim[Im &  idx1 & !idx2 &  idx3,mm] + vim[Im &  idx1 & !idx2 &  idx3,mm-1])
  }
  
  verrsum <- -rowSums(vim*as.numeric(vim<0))
  ferrsum <- rowSums(ferr)
  
  errlst_erridx <- sort(verrsum + ferrsum, decreasing = TRUE, index.return = TRUE)
  errlst <- errlst_erridx$x
  erridx <- errlst_erridx$ix
  
  sel_err <- errlst[1]
  sel_idx <- erridx[1+bpp]

  return(list(sel_idx, sel_err))
}

subproblem <- function(y, size_alpha, gamma, anew, aold){
  N <- size_alpha[[1]]
  M <- size_alpha[[2]]
  aold <- as.numeric(y==-1) + y * gamma
  
  H <- pracma::eye(M)
  f <- c(-anew)
  
  A <- -H + rbind(pracma::zeros(1,M), H[1:M-1,])
  
  if(y == -1) A <- t(A)
  ub <- as.numeric(y == -1) + y * gamma
  
  #alpha_new <- pracma::quadprog(H,f,A, pracma::zeros(M,1), ub = ub)
  
  alpha_new <- modopt.matlab::quadprog(H = H,f = f,A = A, b = pracma::zeros(M,1), ub = ub, x0 = aold)
  
  
  #return (alpha_new$xmin)
  return (alpha_new$x)
}